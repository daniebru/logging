heat_template_version: 2013-05-23

description: >
  HOT template to create a new neutron network plus a router to the public
  network, and for deploying two servers into the new network. The template also
  assigns floating IP addresses to each server so they are routable from the
  public network.

parameters:
  key_name:
    type: string
    description: Name of keypair to assign to servers
  server_manager:
    type: string
    description: Name of manager
  server_webserver:
    type: string
    description: Name of webserver
  image_ubuntu:
    type: string
    description: Type of image for Ubuntu servers
  flavor_small:
    type: string
    description: Small flavor
  public_net:
    type: string
    description: >
      ID or name of public network for which floating IP addresses will be allocated
  private_net_name:
    type: string
    description: Name of private network to be created
  private_net_cidr:
    type: string
    description: Private network address (CIDR notation)
  private_net_gateway:
    type: string
    description: Private network gateway address
  private_net_pool_start:
    type: string
    description: Start of private network IP address allocation pool
  private_net_pool_end:
    type: string
    description: End of private network IP address allocation pool
  sec_group_linux:
    type: string
    description: Linux sec-group

resources:
  private_net:
    type: OS::Neutron::Net
    properties:
      name: { get_param: private_net_name }

  private_subnet:
    type: OS::Neutron::Subnet
    properties:
      network_id: { get_resource: private_net }
      cidr: { get_param: private_net_cidr }
      gateway_ip: { get_param: private_net_gateway }
      allocation_pools:
        - start: { get_param: private_net_pool_start }
          end: { get_param: private_net_pool_end }

  router:
    type: OS::Neutron::Router
    properties:
      external_gateway_info:
        network: { get_param: public_net }

  router_interface:
    type: OS::Neutron::RouterInterface
    properties:
      router_id: { get_resource: router }
      subnet_id: { get_resource: private_subnet }

  manager:
    type: OS::Nova::Server
    properties:
      name: { get_param: server_manager }
      image: { get_param: image_ubuntu }
      flavor: { get_param: flavor_small }
      key_name: { get_param: key_name }
      networks:
        - port: { get_resource: manager_port }

  manager_port:
    type: OS::Neutron::Port
    properties:
      network_id: { get_resource: private_net }
      security_groups:
        - default
        - { get_param: sec_group_linux }
      fixed_ips:
        - subnet_id: { get_resource: private_subnet }

  manager_floating_ip:
    type: OS::Neutron::FloatingIP
    properties:
      floating_network: { get_param: public_net }
      port_id: { get_resource: manager_port }

  webserver:
    type: OS::Nova::Server
    properties:
      name: { get_param: server_webserver }
      image: { get_param: image_ubuntu }
      flavor: { get_param: flavor_small }
      key_name: { get_param: key_name }
      networks:
        - port: { get_resource: webserver_port }

  webserver_port:
    type: OS::Neutron::Port
    properties:
      network_id: { get_resource: private_net }
      security_groups:
        - default
        - { get_param: sec_group_linux }
      fixed_ips:
        - subnet_id: { get_resource: private_subnet }

  webserver_floating_ip:
    type: OS::Neutron::FloatingIP
    properties:
      floating_network: { get_param: public_net }
      port_id: { get_resource: webserver_port }

outputs:
  manager_private_ip:
    description: IP address of manager in private network
    value: { get_attr: [ manager, first_address ] }
  manager_public_ip:
    description: Floating IP address of manager in public network
    value: { get_attr: [ manager_floating_ip, floating_ip_address ] }
  webserver_private_ip:
    description: IP address of webserver in private network
    value: { get_attr: [ webserver, first_address ] }
  webserver_public_ip:
    description: Floating IP address of webserver in public network
    value: { get_attr: [ webserver_floating_ip, floating_ip_address ] }
